using System;
using System.Linq;
using System.Text;

namespace homework_5_5
{
    public class StringUtil
    {
        public static string ExtractWordWithMinLength(string text)
        {
            var words = text.Split(" ", StringSplitOptions.RemoveEmptyEntries);

            Array.Sort(words, (o1, o2) => o1.Length >= o2.Length ? 1 : -1);

            return words[0];
        }

        public static string[] ExtractWordsWithMaxLength(string text)
        {
            var words = text.Split(" ", StringSplitOptions.RemoveEmptyEntries);

            Array.Sort(words, (o1, o2) => o1.Length >= o2.Length ? 1 : -1);

            return (from word in words where word.Length == words[^1].Length select word).ToArray();
        }

        public static string RemoveDuplicateSymbols(string text)
        {
            var tmp = text.ToLower();
            var result = new StringBuilder();

            for (var i = 0; i < tmp.Length; i++)
                if (i == 0 || tmp[i] != tmp[i - 1])
                    result.Append(text[i]);

            return result.ToString();
        }
    }
}