using System.Collections.Generic;

namespace homework_5_5
{
    public class MathUtil
    {
        public static bool IsProgression(double[] array)
        {
            return array.Length >= 3 && IsArithmeticProgression(array) | IsGeometricProgression(array);
        }

        public static int AkkermanFunction(int n, int m)
        {
            if (n == 0) return m + 1;
            if (m == 0) return AkkermanFunction(n - 1, 1);
            if (n > 0 && m > 0) return AkkermanFunction(n - 1, AkkermanFunction(n, m - 1));
            
            return AkkermanFunction(n, m);
        }

        private static bool IsArithmeticProgression(IReadOnlyList<double> array)
        {
            var value = array[1] - array[0];

            for (var i = 1; i < array.Count - 1; i++)
                if (!(array[i] + value).Equals(array[i + 1]))
                    return false;

            return true;
        }

        private static bool IsGeometricProgression(IReadOnlyList<double> array)
        {
            var value = array[1] / array[0];

            for (var i = 0; i < array.Count - 1; i++)
                if (!(array[i] * value).Equals(array[i + 1]))
                    return false;

            return true;
        }
    }
}