namespace homework_5_5
{
    public class Matrix
    {
        public static int[,] MultipleMatrixByNumber(int[,] matrix, int number)
        {
            var result = new int[matrix.GetLength(0), matrix.GetLength(1)];

            for (var i = 0; i < matrix.GetLength(0); i++)
            for (var j = 0; j < matrix.GetLength(1); j++)
                result[i, j] = matrix[i, j] * number;

            return result;
        }

        public static int[,] AdditionMatrices(int[,] firstMatrix, int[,] secondMatrix)
        {
            var result = new int[firstMatrix.GetLength(0), firstMatrix.GetLength(1)];

            for (var i = 0; i < firstMatrix.GetLength(0); i++)
            for (var j = 0; j < firstMatrix.GetLength(1); j++)
                result[i, j] = firstMatrix[i, j] + secondMatrix[i, j];

            return result;
        }

        public static int[,] SubtractionMatrices(int[,] firstMatrix, int[,] secondMatrix)
        {
            var result = new int[firstMatrix.GetLength(0), firstMatrix.GetLength(1)];

            for (var i = 0; i < firstMatrix.GetLength(0); i++)
            for (var j = 0; j < firstMatrix.GetLength(1); j++)
                result[i, j] = firstMatrix[i, j] - secondMatrix[i, j];

            return result;
        }

        public static int[,] MultipleMatrices(int[,] firstMatrix, int[,] secondMatrix)
        {
            var result = new int[firstMatrix.GetLength(0), firstMatrix.GetLength(1)];

            for (var i = 0; i < firstMatrix.GetLength(0); i++)
            for (var j = 0; j < secondMatrix.GetLength(1); j++)
            for (var k = 0; k < secondMatrix.GetLength(0); k++)
                result[i, j] += firstMatrix[i, k] * secondMatrix[k, j];

            return result;
        }
    }
}